resource "kubernetes_secret" "api_config" {
	metadata {
		name = "api-config"
		labels = {
			app = "api"
		}
	}

	data = {
		// DON'T CHANGE: Config of the app within k8s
		DATABASE_HOST="postgres-service.default.svc.cluster.local"
		DATABASE_PORT="5432"
		DATABASE_SYNC="true"
		PORT="3000"
		
		// Match the ones in pg-secret.tf
		DATABASE_USER="postgres"
		DATABASE_PASS="docker"
		DATABASE_NAME="nest_db"

		// Modify at your will
		JWT_KEY="I_AM_WEAK"
		JWT_EXPIRATION="60m"
	}

	type = "Opaque"
}