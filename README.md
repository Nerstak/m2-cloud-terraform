# M2 Cloud with Terraform

## Information

### Description

Building a (small) Cloud environment, with an API and a database. It consists in migrating to the Cloud this project: [M2 Api Nest Project: Blogify](https://gitlab.com/Nerstak/m2-api-nest-project). 

Note that the whole containerisation process and configuration is located within the repository of the application, **NOT** this one. The container is available [here](https://hub.docker.com/repository/docker/nerstak/blogify).

### Technologies used

Build with:

- Kubernetes

- Minikube

- Terraform

Application containing:

- PostgreSQL

- NodeJS (NestJS app)

## Features

The application in itself is the API of blog managment website. It comes with a Swagger to help visualize the actions, and interact with the API server.

The application is embedded within a Terraform project, to deploy / destroy into Minikube. There is also the possibility to configure the application to fit the needs.



The goal of the project was mainly to verify the viability of using Terraform to configure and deploy on Minikube. This approach has the advantage of being free, compared to Kubernetes providers (free limited tier at most), which is best to experiment with tools.

## Usage

### Preparation

In `main.tf`, edit the `config_path` to your own Kubeconfig file. The `.kube` folder should be located within the current user directory (for Windows and Unix).

### Initialization

To start Minikube, run `minikube start`.

To enable Ingress, run `minikube addons enable ingress`.

To initialize the Terraform project, run `terraform init`.

### Starting Terraform

Run `terraform apply` to deploy the application in Minikube, and confirm with `yes`.

After this command, run `minikube tunnel`. This allows us to access the Ingress endpoint.

Go to [127.0.0.1:80/api](127.0.0.1:80/api) to see the Swagger page of the API.

## Notes

### Persistance

Persistant data (for the database) are stored in `/mnt/data`. This folder is located within the Minikube container. I do **not** recommand mounting any local folder using `minikube mount ./data:/mnt/data`, it would messes up permissions, which is an issue with Postgres (it requires `700` or `750` on uid and gid `999`, and it is not possible to refine this much with Windows as the primary OS).

Other than that, data are persistant from one run to another. They can be accessed using `minikube ssh`, or using STFP with the SSH credentials of the Minikube container.

### Limitations

Using Minikube as Kubernetes implementation definitly has its issues:

- No actual production usage (compared to GKE or other providers)

- Issues with Windows: mounting volumes is complex if applications requires very specific permissions.

### Secrets

Secrets for PostgreSQL and the application are currently exposed. It does not really matter, as there is no deployment online: this application is designed to be deployed locally!

However, it would be best to keep them outside of the code repository if this project was to move to an actual cloud provider.
