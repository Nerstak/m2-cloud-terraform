resource "kubernetes_secret" "postgres_config" {
	metadata {
		name = "postgres-config"
		labels = {
			app = "postgres"
		}
	}

	data = {
		POSTGRES_DB = "nest_db"
		POSTGRES_USER = "postgres"
		POSTGRES_PASSWORD = "docker"
	}

	type = "Opaque"
}