terraform {
	required_providers {
		kubernetes = {
			source  = "hashicorp/kubernetes"
			version = ">= 2.7.0"
		}
	}
}
provider "kubernetes" {
	config_path = "C:/Users/Karsten/.kube/config"
}

resource "kubernetes_deployment" "api_deployment" {
	metadata {
		name = "api-deployment"
	}

	spec {
		replicas = 1
		selector {
			match_labels = {
				app = "api"
			}
		}
		template {
			metadata {
				labels = {
					app = "api"
				}
			}
			spec {
				container {
					image = "nerstak/blogify"
					name  = "api-container"
					env_from {
						secret_ref {
							name = "api-config"
						}
					}
				}
			}
		}
	}
}

resource "kubernetes_service" "api_service" {
	metadata {
		name = "api-service"
	}

	spec {
		selector = {
			app = "api"
		}
		port {
			name = "http"
			port        = 80
			target_port = 3000
		}
		type = "ClusterIP"
	}
}

resource "kubernetes_ingress_v1" "api_ingress" {
	metadata {
		name = "api-ingress"
		annotations = {
			"kubernetes.io/ingress.class" = "nginx"
		}
	}
	spec {
		rule {
			http {
				path {
					backend {
						service {
							name = "api-service"
							port {
								number = 80
							}
						}
					}
				}
			}
		}
	}
}

resource "kubernetes_persistent_volume" "postgres_pv" {
	metadata {
		name = "postgres-pv"
		labels = {
            type = "local"
        }
	}
	spec {
		storage_class_name = "manual"
		capacity = {
			storage = "100M"
		}
		access_modes = ["ReadWriteOnce"]
		persistent_volume_source {
			host_path {
				path = "/mnt/data"
			}
		}
	}
}

resource  "kubernetes_persistent_volume_claim" "postgres_pvc" {
  metadata {
    name = "postgres-pvc"
	labels = {
		app = "postgres"
	}
  }
  spec {
	storage_class_name = "manual"
	access_modes = ["ReadWriteOnce"]
	resources {
	  requests = {
		storage = "100M"
	  }
	}
  }
}

resource "kubernetes_deployment" "postgres_deployment" {
	metadata {
		name = "postgres-deployment"
	}

	spec {
		replicas = 1
		selector {
			match_labels = {
				app = "postgres"
			}
		}
		template {
			metadata {
				labels = {
					app = "postgres"
				}
			}
			spec {
				volume {
					name = "postgres-db"
					persistent_volume_claim {
						claim_name = "postgres-pvc"
					}
				}
				container {
					name  = "postgres-container"
					image = "postgres:latest"
					image_pull_policy = "Always"
					port {
						container_port = 5432
					}	
					env_from {
						secret_ref {
							name = "postgres-config"
						}
					}
					volume_mount {
						mount_path = "/var/lib/postgresql/data"
						name = "postgres-db"
					}
				}
			}
		}
	}
}

resource "kubernetes_service" "postgres_service" {
	metadata {
		name = "postgres-service"
	}

	spec {
		selector = {
			app = "postgres"
		}
		port {
			name = "postgres"
			port = 5432
		}
		type = "ClusterIP"
	}
}